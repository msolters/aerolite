int r = 3;
int g = 5;
int b = 6;

// char array material
int buffSize = 19;
boolean recording = false;
char inData[20]; // Allocate some space for the string
char inChar=-1; // Where to store the character read
byte index = 0; // Index into array; where to store the character


String msg = ""; 
char character;  // the setup routine runs once when you press reset: 

void setup() {   
  Serial.begin(9600);     // initialize the digital pin as an output.   
  pinMode(r, OUTPUT);   
  pinMode(g, OUTPUT);   
  pinMode(b, OUTPUT);      
  establishContact();
  delay(10);
}  


void extract() {
    while (Serial.available() > 0) {
        if(index < buffSize) {
            inChar = Serial.read(); // Read a character
            //Serial.println(inChar);
            if (inChar == '$') {
              
              if (recording == false) {
                //Serial.println("recording");
                recording = true;
                continue;
              } else {
                //Serial.println("not recording");
                recording = false;
                msg = String(inData);
                set_state();
                continue;
              }
            }
            
            if (recording == true) {
              inData[index] = inChar; // Store it
              index++; // Increment where to write next
              inData[index] = '\0'; // Null terminate the string
            }
        } else {
            set_state();
        }
    }
}

void set_state() {
  index=0;
  //Serial.println(msg);
  Serial.flush();
  set_led_state(msg);
  msg = "";
}

void loop() {
  extract();
}

void set_led_state(String msg) {
  if (msg != "") { 
    String red =  getValue(getValue(msg, ';', 0), ':', 1);
    int R = red.toInt();
    String green =  getValue(getValue(msg, ';', 1), ':', 1);
    int G = green.toInt();
    String blue =  getValue(getValue(msg, ';', 2), ':', 1);
    int B = blue.toInt();
    
    //Serial.println(msg);
    analogWrite(r, R);
    analogWrite(g, G);
    analogWrite(b, B);
   }
}


void establishContact() {
  Serial.println("Deploying beacon.");
  while (Serial.available() <= 0) {     
    Serial.println("0,0,0");   // send an initial string     
    delay(300);
  }
  Serial.println("Device found.");
  Serial.flush();
}

String getValue(String data, char separator, int index) {
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }

  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}
