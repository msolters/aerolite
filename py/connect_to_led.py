import socket, time, serial
import threading, Queue

HOST = 'localhost'  # Symbolic name meaning the local host
PORT = 24069    # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))
q = Queue.Queue(3)

ard = serial.Serial('/dev/ttyACM0', 9600)

print 'Searching for Arduino serial port...'
while True:
    beacon = ard.readline()
    print 'looking for beacon: ', beacon
    ard.write('hi')
    time.sleep(0.01)
    if "Device" in beacon:
        break

def set_led(msg):
    print 'outgoing to arduino: ', msg
    ard.write(msg)
    #time.sleep(0.01)
    #ard.flush()
    #print 'recieved from arduino: '
   
    #ard.flush()

def update_data(q):
    print '**************** Starting data download thread. *************'
    while True:
        s.send('GET')
        reply = s.recv(2048)
        try:
            if '-' not in reply:
                q.put(reply, False)
        except:
            pass
        time.sleep(0)

def render_led(q):
    print '**************** Starting led management thread. *************'
    while True:
        try:
            set_led(q.get(False))
        except:
            pass
        time.sleep(0.01)

threading.Thread(target=render_led, args=(q,)).start()
threading.Thread(target=update_data, args=(q,)).start()
