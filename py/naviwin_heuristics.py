# classes used for gesture heuristic construction

import time

class timedDecision(object):

    trueCount = 0
    falseCount = 0

    def __init__(self, length):
        self.length = length # number of seconds of unbroken input before yes is returned
        self.metaTimeTrue = 0
        self.metaTimeFalse = 0
        self.state = False # on or off

    def update(self, inputBool):
        now = time.time()
        if inputBool is True:
            if self.trueCount == 0:
                self.metaTimeTrue = time.time() # always document the beginning of a streak
            elif self.trueCount > 0 and now - self.metaTimeTrue > self.length:
                self.trueCount = 0 #reset counter
                return True
            self.trueCount += 1


        else:
            self.trueCount = 0

        return False

class trigger(object):
    switchV = False

    onSwitch = timedDecision(0.03)
    offSwitch = timedDecision(0.02)

    offfSwitch = timedDecision(1.5)

    def check(self, inputBoolOn, inputBoolOff):
        onC = self.onSwitch.update(inputBoolOn)
        offC = self.offSwitch.update(inputBoolOff)

        #print onC, offC
        if onC is True and self.switchV is False:
            self.switchV = True
            return True
        if offC is True and self.switchV is True:
            self.switchV = False
            return False
        else:
            return None

class returnAvg(object):  #this algorithm returns an equally weighted average of the last N=histSize data points

    def __init__(self, histSize): #this code invokes a new instance of the algorithm with N=histSize
        self.N = int(histSize) #this is the number of data points to average
        self.cVec = []
        self.currAvg = None
        self.length = 0

    def addData(self, c):  #invoke this method to add a new data point, and return the recomputed average
        self.cVec.append(float(c)) #add data point to end of history vector
        self.length = len(self.cVec) #calculate new length of vector

        if self.length > self.N: #only keep last self.N data points
            self.cVec.pop(0) #delete the first (oldest) element

        if self.length > 1: #prevent division by 0 in the case where histSize \def 1 (cVec is a scalar)
            T = sum(element for element in self.cVec)
            self.currAvg = T/(self.length-1) #compute average
        else:
            self.currAvg = T = self.cVec[0]
        return self.currAvg

class inertialResponse(object):

    mu = 0.9 # decay in seconds
    curr_speed = 0.0
    curr_speed_wraith = 0.0
    metaTime = 0.0

    def stop(self):
        self.curr_speed = 0.0

    def boost(self, gamma):
        self.curr_speed = gamma
        self.curr_speed_wraith = self.curr_speed
        self.metaTime = time.time()

    def calculate(self):
        if abs(self.curr_speed) > 0:
            now = time.time()
            self.curr_speed = self.curr_speed_wraith * math.exp(-(now - self.metaTime)/self.mu)
        return self.curr_speed
