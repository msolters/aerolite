import socket, sys, time
import threading, Queue

#r = g = b = 0

HOST = ''   # Symbolic name meaning the local host
PORT = 24069    # Arbitrary non-privileged port
s = socket.socket()#socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
print 'Socket created'
try:
    s.bind((HOST, PORT))
except socket.error , msg:
    print 'Bind failed. Error code: ' + str(msg[0]) + 'Error message: ' + msg[1]
    sys.exit()
print 'Socket bind complete'
s.listen(2)
print 'Socket now listening'

q = Queue.Queue(25)



def clientthread(conn, addr, q):
    print '******************new thread'
    reply = ''
    print 'Connected with ' + addr[0] + ':' + str(addr[1])
    
    while True:
        
    # RECEIVE DATA
        data = conn.recv(1024)

    # PROCESS DATA
        #print stored_data
        tokens = data.split(' ',1)            # Split by space at most once
        command = tokens[0]                   # The first token is the command
        if command=='GET':                    # The client requests the data   
            foo = True
            while foo:
                try:
                    d = q.get(False)
                    if (len(reply) + len(d) ) < 2048:
                        reply += d  + ','             # Return the stored data
                    else:
                        #print 'data', reply
                        print '\t downloading' 
                        conn.send(reply)
                        reply = d
                        foo = False 
                except:
                    if len(reply) > 0:
                        #print 'data', reply
                        print '\t downloading' 
                        conn.send(reply)
                        reply = ''
                    else:
                        #print 'data -'
                        conn.send('-')
                    foo=False
            
        elif command=='STORE':                # The client want to store data
            q.put(tokens[1])     # Get the data as second token, save it
            print 'uploading'
            reply = 'OK'
            conn.send(reply)                     # Acknowledge that we have stored the data
        #else:
            #reply = 'Unknown command'
            #conn.send(reply)

        
        time.sleep(0)


while True:
    (conn, addr) = s.accept()
    try:
        threading.Thread(target=clientthread, args=(conn, addr, q)).start()
    except:
        print 'thread collapsed.'

conn.close() # When we are out of the loop, we're done, close
s.close()
