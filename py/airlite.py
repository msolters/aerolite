# author: mark j solters
# created december 10th, 2013

import Leap, sys, socket
import threading, Queue
import time, math
from Leap import CircleGesture, KeyTapGesture, ScreenTapGesture, SwipeGesture
from naviwin_heuristics import *

HOST = ''  # Symbolic name meaning the local host
PORT = 24069    # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))

outgoing_q = Queue.Queue(35)

def send_msg(msg):
    print 'outgoing: ', msg
    #ard.write(msg)
    s.send('STORE '+msg)
    print str(s.recv(1024))

class hailSithis(Leap.Listener):

    meta_pos = None
    d_hist = returnAvg(50)

    frontHandID = None
    
    x = y = z = 0

    def on_init(self, controller):
        print "The listener is ready."

    def on_connect(self, controller):
        print "The Void speaks..."

    def on_disconnect(self, controller):
        print "Disconnected"

    def on_exit(self, controller):
        print "exited"

    def on_frame(self, controller):  #gesture processing goes here
        frame=controller.frame()
        nHands = len(frame.hands)

        #print self.closed
        if frame.hands.is_empty: #first we check that we have hands
            pass
        else:  # there are hands in the frame
            if nHands > 0:
                h_1 = frame.hands.frontmost
                #h_2 = frame.hands[1]
                iBox = frame.interaction_box
                stabilized_h_1 = h_1.stabilized_palm_position
                psi = iBox.normalize_point(stabilized_h_1)
                if self.meta_pos is not None:
                    d = stabilized_h_1 - self.meta_pos
                    d = d.magnitude
                    R = self.d_hist.addData(d)
                    
                    print R
                    if R > 1.0:
                        try:
                            outgoing_q.put('$r:'+str(int(abs(255*psi.x)))+';'+'g:'+str(int(abs(255*psi.y)))+';'+'b:'+str(int(abs(255*psi.z)))+"$", False)            
                        except:
                            print 'server queue full' 
                self.meta_pos = stabilized_h_1
              

def upload_data(q):
    while True:
        msg = ''
        
        while q.empty() is False:
            msg += q.get(False) + ','
        msg = msg[:-1]
        if len(msg) > 0:
            send_msg(msg)
        time.sleep(0)


def main(): #main program loop

    listener = hailSithis()
    controller = Leap.Controller()
    controller.set_policy_flags(Leap.Controller.POLICY_BACKGROUND_FRAMES) #allows frame data to be gathered even when window is not in focus
    controller.add_listener(listener)
    threading.Thread(target=upload_data, args=(outgoing_q, )).start()
    
    print "press Enter to quit"
    sys.stdin.readline()
    #time.sleep(240)
    controller.remove_listener(listener)

main()
